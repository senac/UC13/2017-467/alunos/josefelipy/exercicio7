package br.com.senac.ex7;

public class Circulo extends Area{

private final double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public double getArea() {
            return (this.raio * this.raio) * 3.14;
    }


    
}
