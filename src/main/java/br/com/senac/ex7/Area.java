package br.com.senac.ex7;

public abstract class Area {
    
    
    public abstract double getArea();
    
    
}
