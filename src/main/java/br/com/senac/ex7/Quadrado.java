package br.com.senac.ex7;

public class Quadrado extends Area{

private final double lado;

    public Quadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double getArea() {
    
        return this.lado * this.lado;
    }



    
}
