/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex7;

/**
 *
 * @author Administrador
 */
public class Retangulo extends Area{

    private final double ladoA;
    private final double ladoB;

    public Retangulo(double ladoA, double ladoB) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
    }
    
    
    @Override
    public double getArea() {
        return this.ladoA * this.ladoB;
    }

    
}
