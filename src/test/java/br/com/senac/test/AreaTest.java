
package br.com.senac.test;

import br.com.senac.ex7.Triangulo;
import br.com.senac.ex7.Retangulo;
import br.com.senac.ex7.Circulo;
import br.com.senac.ex7.Quadrado;
import org.junit.Test;
import static org.junit.Assert.*;

public class AreaTest {
    
    public AreaTest() {
    }
   
    @Test
    public void areaDeUmQuadrado(){
        Quadrado quadrado = new Quadrado(10);
        
        assertEquals(100 , quadrado.getArea(), 0.1);
        
    }
    @Test
    public void areaDeUmCirculo(){
        Circulo circulo = new Circulo(5);
        
        assertEquals(78.5, circulo.getArea(), 0.1);
    }
    @Test
    public void areaDeUmRetangulo(){
        Retangulo retangulo = new Retangulo(10,20);
        
        assertEquals(200, retangulo.getArea(), 0.1);
        
    }
    @Test
    public void areaDeUmTriangulo(){
        Triangulo triangulo = new Triangulo(30, 20);
        
        assertEquals(300, triangulo.getArea(), 0.1);
    }
    
    @Test
    public void retanguloNaoDeveSerUmQuadrado(){
        Retangulo retangulo = new Retangulo(10, 20);
        Quadrado quadrado = new Quadrado(10);
        assertNotEquals(quadrado.getArea(), retangulo.getArea() , 0.1);
    }
   
}
